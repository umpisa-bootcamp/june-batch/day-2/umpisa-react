import React from 'react';
import TableRow from '../TableRow';
import H1 from '../H1';

export default function Table({
    title = "",
    data = [],
    onClickRow = () => {},
    onClickDelete = () => {}
}) {
    return (
        <div>
        <H1>{title}</H1>
        <table>
            <thead>
                <tr>
                    <th>Maker</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {data.map((singleData) =>
                    <TableRow onClickDelete={onClickDelete} onClickRow={onClickRow} appliance={singleData} key={`appliance_table_row_${singleData.name}`} />)}
            </tbody>
        </table>
    </div>
    )
}