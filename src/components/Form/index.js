import React, {useState, useEffect} from 'react';

export default function Form({
    onSubmit,
    initialValues,
}) {

    const [state, setState] = useState(initialValues)

    useEffect(() => {
        setState(initialValues)
    }, [initialValues])

    const onChangeField = (field, event) => {
        setState({ ...state, [field]: event.target.value })
    }

    return (
        <form onSubmit={() => onSubmit({ name: state.name, maker: state.maker })}>
            <div>
                <h5>Maker</h5>
                <input type="text" value={state.maker} onChange={(e) => onChangeField('maker', e)} />
            </div>
            <div>
                <h5>Name</h5>
                <input type="text" value={state.name} onChange={(e) => onChangeField('name', e)} />
            </div>
            <button>Submit</button>
        </form>
    )
}