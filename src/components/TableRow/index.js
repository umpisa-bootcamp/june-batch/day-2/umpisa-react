import React from 'react';

export default function TableRow({ appliance, onClickRow, onClickDelete }) {
    return (
        <tr>
            <td style={{textAlign: 'left'}} onClick={() => onClickRow(appliance)}>{appliance.maker}</td>
            <td onClick={() => onClickRow(appliance)}>{appliance.name}</td>
            <td onClick={() => onClickDelete(appliance.id)}>x</td>
        </tr>
    )
}