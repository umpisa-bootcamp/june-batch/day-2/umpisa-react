import React, { useEffect, useState } from 'react';
import './styles.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { createBrowserHistory } from "history";
import Home from '../Home';
import ApplianceFormTable from '../ApplianceFormTable';

function App() {
    const history = createBrowserHistory();
  return (
    <Router history={history}>
        <div>
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                </ul>
            </nav>

            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/edit" component={ApplianceFormTable} />
            </Switch>
        </div>
    </Router>
  );
}

export default App;
