import React, { useEffect, useState } from 'react';
import './styles.css';
import Table from '../../components/Table';


function Home({
    history
}) {

  const [state, setState] = useState({
      appliances: [],
      selectedAppliance: {
          name: '',
          id: '',
          maker: ''
      }
  })

  const deleteAppliance = async (id) => {
    await fetch(`http://localhost:3001/v1/appliances/${id}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })

    await fetchAppliances()
  }

  const fetchAppliances = async () => {
    const res = await fetch('http://localhost:3001/v1/appliances', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
    const response = await res.json()
    setState({...state, appliances: response.data})
  }


  const onClickApplianceRow = (data) => {
    history.push('/edit', data)
  }

  useEffect(() => {
    fetchAppliances()
  }, [history.location?.state?.saved])

  return (
    <div className="App">
      <header className="App-header">
        <Table onClickDelete={deleteAppliance} onClickRow={onClickApplianceRow} title="Appliances Table" data={state.appliances} />
      </header>
    </div>
  );
}

export default Home;
