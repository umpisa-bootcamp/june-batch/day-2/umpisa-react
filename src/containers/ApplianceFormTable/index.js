import React, {useState} from 'react';
import Form from '../../components/Form';

function ApplianceFormTable({ history }) {
    const selectedAppliance = history.location.state
    console.warn("selected", history)
    const createAppliance = async (data) => {
      await fetch('http://localhost:3001/v1/appliances', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      })
    }

    const editAppliance = async (id, data) => {
      await fetch(`http://localhost:3001/v1/appliances/${id}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
      })
    }

    const onSubmit = (data) => {
        if (selectedAppliance.id) {
          editAppliance(selectedAppliance.id, data)
        } else {
          createAppliance(data)
        }
        history.push('/', {saved: true})
    }

    return (
      <div className="App">
        <header className="App-header">
          <Form onSubmit={onSubmit} initialValues={selectedAppliance} />
        </header>
      </div>
    );
  }

  export default ApplianceFormTable;